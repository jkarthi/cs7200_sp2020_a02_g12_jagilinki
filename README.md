# CS7200_SP2020_A02_G12_JAGILINKI

Summary:
The Linear Discriminant Analysis algorithm is implemented with feature of 150 data points and 3 classes. Data is generated to meet the assumptions,
i.	Data is Gaussian, verify with data plots
ii.	Verify each variable has same variance 
iii.	Remove outliers
Classes 0, 1, 2 are drawn from a Gaussian distribution of mean 0, standard deviation 1
Data is then, Scaled and Normalized data using Standard Scaler and verified by plotting on graph.
 
Using sklearn KFold cross validation, performed 5 fold cross validation of data for a variety of alpha values where data is splitted between training and testing data sets. Computed discriminant function value for each of the classes y0, y1, y2 and compared to get the maximum index value and further predicted the classification metrics. Below are the classification metrics report for each fold, followed by confusion matrix.
Results:  11
              precision    recall  f1-score   support

           0       0.00      0.00      0.00         8
           1       0.37      1.00      0.54        11
           2       0.00      0.00      0.00        11

    accuracy                           0.37        30
   macro avg       0.12      0.33      0.18        30
weighted avg       0.13      0.37      0.20        30

Results:  9
              precision    recall  f1-score   support

           0       0.00      0.00      0.00        11
           1       0.30      1.00      0.46         9
           2       0.00      0.00      0.00        10

    accuracy                           0.30        30
   macro avg       0.10      0.33      0.15        30
weighted avg       0.09      0.30      0.14        30

Results:  12
              precision    recall  f1-score   support

           0       0.00      0.00      0.00         9
           1       0.40      1.00      0.57        12
           2       0.00      0.00      0.00         9

    accuracy                           0.40        30
   macro avg       0.13      0.33      0.19        30
weighted avg       0.16      0.40      0.23        30

Results:  6
              precision    recall  f1-score   support

           0       0.20      1.00      0.33         6
           1       0.00      0.00      0.00        14
           2       0.00      0.00      0.00        10

    accuracy                           0.20        30
   macro avg       0.07      0.33      0.11        30
weighted avg       0.04      0.20      0.07        30

Results:  9
              precision    recall  f1-score   support

           0       0.00      0.00      0.00        16
           1       0.30      1.00      0.46         9
           2       0.00      0.00      0.00         5

    accuracy                           0.30        30
   macro avg       0.10      0.33      0.15        30
weighted avg       0.09      0.30      0.14        30
 

Reflection:
Key takeaways from the week includes, understanding of preprocessing steps, assumptions verification, learning to approach from the mathematical standpoint.
My initial approach to implementation of discriminant function was using python list and dictionaries. 
Processing data by iterating through dictionaries was causing lot more computations when need to find the index value of the maximum value of three classes. 
The code has become much fine-tuned after applying matrix operations using panda data frames/panda library. 

